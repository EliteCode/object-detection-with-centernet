**Object Detection with CenterNet**

The repo is modified version of [ orignal repo](https://github.com/xingyizhou/CenterNet) by xingyizhou. Read the repo for complete installation and details.

Installation from Here :

1: conda env create -f env.yml


The [ik.py](https://gitlab.com/EliteCode/object-detection-with-centernet/-/blob/master/src/ik.py) is script written to facilate to use in your own code. The details of each step is mention with code comments. 

Few changes are made to get detected image that can be used for further processing according to ons own application( algorithm).

**Changes in orignal rep:**

1. Image show is [disabled ](https://gitlab.com/EliteCode/object-detection-with-centernet/-/blob/master/src/lib/detectors/ctdet.py#L96) and [returned with BBox](https://gitlab.com/EliteCode/object-detection-with-centernet/-/blob/master/src/lib/detectors/ctdet.py#L98)  to use in your own code .
2. The [get_image()](https://gitlab.com/EliteCode/object-detection-with-centernet/-/blob/master/src/lib/utils/debugger.py#L85) method is added in the `debugger.py` to get proccesd image at any time.

