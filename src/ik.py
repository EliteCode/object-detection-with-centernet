#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 22 20:03:18 2020

@author: Ikram ullah Khan
"""
import _init_paths
import sys
import os
from opts import opts
from detectors.detector_factory import detector_factory
import datetime
import cv2

#Example input video and Image Format . Taken from orignal code
image_ext = ['jpg', 'jpeg', 'png', 'webp']
video_ext = ['mp4', 'mov', 'avi', 'mkv']

def cls_screen(): # clear Screen IPython console 
    os.system('cls' if os.name=='nt' else 'clear')

"""
Model is initialized here, the trained weights of coco dta set are downloaded and 
placed in Location Specified by Model Path.
opts are initilzed and required parameters are set that would be used at different 
methods in different objects.
the Repo contain multiple detector in src/lib/detector/, the default is ctdet which 
in out case is outr task so we didnot assign the value to opt_task before passing to 
detector_factory.
after seting up the model we returned it to use in either image detection or video detection. 
"""    
def model_initialze():
    
    CENTERNET_ROOT = '/home/coder/object-detection-with-centernet/src/lib'
    sys.path.insert(0, CENTERNET_ROOT)
  
    MODEL_PREFIX = '/home/coder/object-detection-with-centernet/models/ctdet_coco_dla_2x.pth'
    opt = opts().init()
    opt.load_model=MODEL_PREFIX
    opt.debug = 1
    Detector = detector_factory[opt.task] #default opt.task=ctdet
    detector = Detector(opt)
    return detector

#Method for image detection , required proper initialized model as argument.
#IMG_PREFIX is initialized inside the method considering the image at image folder 
#can be passed as argument.
def image_detect(detector):
    cls_screen()
    IMG_PREFIX = '/home/coder/object-detection-with-centernet/images/testimage.jpg'
    if (os.path.isdir(IMG_PREFIX)):
        image_names = []
        ls = os.listdir(IMG_PREFIX)
        for file_name in sorted(ls):
            ext = file_name[file_name.rfind('.') + 1:].lower()
            if ext in image_ext:
                image_names.append(os.path.join(IMG_PREFIX , file_name))
    else: 
        image_names=[IMG_PREFIX]
    for (image_name) in image_names:
      image,ret = detector.run(image_name)
    return ret 
"""
The video_detect will perform the assigned task. the Model with pre-trained weights is initilzed and 
then passed as argument to method. The test_vid is video under test on which we will perform 
detection. The method will check format of input video, if input format is from example input format
the detection will be performed.The resulted video will be saved in the  same directory of input video
with same property as input video i.e FPS, SIZE,Format.The video is named as output with current time 
e.g Output20200928135450.mp4. 
"""
def video_detect(detector):
    cls_screen()
    VID_PREFIX='/home/coder/object-detection-with-centernet/videos/test_vid.mp4'
    Head, file = os.path.split(VID_PREFIX)
    root, ext = os.path.splitext(VID_PREFIX)
    ext = ext[1:].lower()
    if ext in video_ext:
        cap = cv2.VideoCapture(VID_PREFIX)
        fps = cap.get(5)
        fwidth = int(cap.get(3))
        fheight = int(cap.get(4))
        now = datetime.datetime.now()
        OUTPUT_PREFIX = os.path.join(Head, 'Output'+now.strftime("%Y%m%d%H%M%S")+'.'+ext)
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        output = cv2.VideoWriter(OUTPUT_PREFIX, fourcc, fps, (fwidth, fheight))   
        detector.pause = False
        while cap.isOpened(): #check if oject is properly initialzed
            ret, frame = cap.read() 
            if ret==True: # only perform opertion when we have valid frame
                image,ret = detector.run(frame) # small changes are made to use the orignal repo functionality.
                output.write(image)         #Image packed back to back for video generation
                #cv2.imshow('Detections', image) #optionaly can see the detected result.remove # in front to see result 
                #Pressing q will stop the loop 
                if cv2.waitKey(1) & 0xFF == ord('q'): 
                    break
            else :
                break
        #Releasing resources    
        cap.release()   
        output.release()
        cv2.destroyAllWindows()
    else :
        print('Video not in Required format')
    

if __name__ == '__main__':
  sys.argv = ['centernet', 'ctdet']
  detector=model_initialze()
  
  video_detect(detector)
  #ret=image_detect(detector)